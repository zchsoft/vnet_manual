#!/bin/bash

if [ $# -lt 3 ]
then
  echo "Usage: $0 dhcp_index switch_index ip"
  exit 1
fi

dhcp_index=$1
switch_index=$2
ip=$3

port_index=$dhcp_index

dhcp_name=DHCPServer$dhcp_index
switch_name=Switch$switch_index
port_name=dhcp-veth$port_index

./add_port.sh "$dhcp_name" "$switch_name" "$port_name" "$ip"
