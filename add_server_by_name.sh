#!/bin/bash

server_name=$1

ip netns add $server_name
ip netns exec $server_name ip link set lo up
