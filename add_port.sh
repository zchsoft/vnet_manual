#!/bin/bash

if [ $# -lt 4 ]
then
  echo "Usage: $0 server_name switch_name port_name ip"
  exit 1
fi

server_name=$1
switch_name=$2
port_name=$3
ip=$4

server_port=${port_name}-vm
switch_port=${port_name}-br

ip link add $server_port type veth peer name $switch_port

brctl addif $switch_name $switch_port
ip link set $switch_port up

ip link set $server_port netns $server_name
if [ -n "$ip" ]
then
ip netns exec $server_name ip address add $ip dev $server_port
fi
ip netns exec $server_name ip link set $server_port up
