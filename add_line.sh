#!/bin/bash

line_name=$1

line_enda=${line_name}_a
line_endb=${line_name}_b

ip link add ${line_enda} type veth peer name ${line_endb}
ip link set ${line_enda} up
ip link set ${line_endb} up
