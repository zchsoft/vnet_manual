#!/bin/bash

if [ $# -lt 3 ]
then
  echo "Usage: $0 server_index switch_index ip"
  exit 1
fi

server_index=$1
switch_index=$2
ip=$3

port_index=$server_index

server_name=Server$server_index
switch_name=Switch$switch_index
port_name=veth$port_index

./add_port.sh "$server_name" "$switch_name" "$port_name" "$ip"
