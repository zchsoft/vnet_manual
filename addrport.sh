#!/bin/bash

if [ $# -lt 2 ]
then
  echo "Usage: $0 switch_index ip"
  exit 1
fi

switch_index=$1
ip=$2

port_index=$switch_index

router_name=Router
switch_name=Switch$switch_index
port_name=veth$port_index

./add_port.sh "$router_name" "$switch_name" "$port_name" "$ip"
