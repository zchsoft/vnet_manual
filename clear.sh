#!/bin/bash

ip link delete veth11-br
ip link delete veth12-br
ip link delete veth13-br
ip link set Switch1 down
brctl delbr Switch1
ip netns del Server11
ip netns del Server12
ip netns del Server13

ip link delete veth21-br
ip link delete veth22-br
ip link set Switch2 down
brctl delbr Switch2
ip netns del Server21
ip netns del Server22

ip link delete veth1-br
ip link delete veth2-br
ip netns del Router

ip netns del DHCPServer1

dnsmasq1=$(ps -ef | grep dnsmasq | grep dhcp1 | grep -v grep| awk '{print $2}') 
if [ -n "$dnsmasq1" ]
then
    kill $dnsmasq1
fi

dhclient13=$(ps -ef | grep dhclient | grep veth13-vm | grep -v grep| awk '{print $2}')
if [ -n "$dhclient13" ]
then
    kill $dhclient13
fi

ip link delete line1_b
ip link delete line2_a

